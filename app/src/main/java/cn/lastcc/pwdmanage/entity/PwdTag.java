package cn.lastcc.pwdmanage.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * 密码标签实体类
 *
 * @author Added by baidongyang 2013-6-23
 * @version 13.6.23.1
 *
 */
public class PwdTag implements Serializable {

    private static final long serialVersionUID = -8214574211338479559L;

    private String  id;
    private String tagName;
    private String pwd;
    private byte[] salt;
    private Date createDate;
    private Date modifyDate;
    private int modifyTimes;

    /**
     * 创建该对象时，自动生成 ID
     */
    public PwdTag(){
    	this.id=UUID.randomUUID().toString();
    }

    /**
     * 获取属性：ID(预留)
     * @return
     * @author Added by baidongyang MIS- 2013-6-23
     */
    public String getId() {
        return id;
    }

    /**
     * 设置属性：ID (预留)
     * @param id
     * @author Added by baidongyang 2013-6-23
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取属性：标签名称
     * @return
     * @author Added by baidongyang 2013-6-23
     */
    public String getTagName() {
        return tagName;
    }

    /**
     * 设置属性：标签名称
     * @param tagName
     * @author Added by baidongyang 2013-6-23
     */
    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    /**
     * 获取属性：盐值
     * @return
     * @author Added by baidongyang 2019-2-6
     */
    public byte[] getSalt() {
        return salt;
    }

    /**
     * 设置属性：盐值
     * @param salt
     * @author Added by baidongyang 2019-2-6
     */
    public void setSalt(byte[] salt) {
        this.salt = salt;
    }

    /**
     * 获取属性：密码
     * @return
     * @author Added by baidongyang 2013-6-23
     */
    public String getPwd() {
        return pwd;
    }

    /**
     * 设置属性：密码
     * @param pwd
     * @author Added by baidongyang 2013-6-23
     */
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    /**
     * 获取属性：创建时间
     * @return
     * @author Added by baidongyang 2013-6-23
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置属性：创建时间
     * @param createDate
     * @author Added by baidongyang 2013-6-23
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取属性：最后修改时间（可以作为时间戳）
     * @return
     * @author Added by baidongyang 2013-6-23
     */
    public Date getModifyDate() {
        return modifyDate;
    }

    /**
     * 设置属性：最后修改时间
     * @param modifyDate
     * @author Added by baidongyang 2013-6-23
     */
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    /**
     * 获取属性：修改次数
     * @return
     * @author Added by baidongyang 2013-6-23
     */
    public int getModifyTimes() {
        return modifyTimes;
    }

    /**
     * 设置属性：最后修改次数
     * @param modifyTimes
     * @author Added by baidongyang 2013-6-23
     */
    public void setModifyTimes(int modifyTimes) {
        this.modifyTimes = modifyTimes;
    }
}
