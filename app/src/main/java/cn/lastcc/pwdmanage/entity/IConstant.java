package cn.lastcc.pwdmanage.entity;

import android.os.Environment;

import java.io.File;

/**
 * 常量类
 *
 * @author Added by baidongyang 2013-6-23
 * @version 13.6.23.1
 *
 */
public class IConstant {

    public final static String TAG = "PwdManager";
    public final static String INFO_FILE_NAME = "info.bin";
    public final static String LOGIN_PARAM_PWD_LBL = "logonPwd";
    /** 密码文件版本，每次变更密码文件格式或存储的内容格式时需要修改 */
    public final static String INFO_CUR_VERSION_STR = "3";
    public final static int INFO_CUR_VERSION_DIGIT = Integer.parseInt(INFO_CUR_VERSION_STR);
    public final static String INFO_VERSION_3_STR = "3";
    public final static int INFO_VERSION_3_DIGIT = Integer.parseInt(INFO_VERSION_3_STR);

    /** 每个密码盐值的长度(byte) */
    public final static int LENGTH_SALT = 32;

    /** 导入密码文件的路径 */
    public final static String PWD_IMPORT_PATH = Environment.getExternalStorageState()
            + File.separator + "pwd_imp.txt";

    /** 导出文件的版本 */
    public final static int EXPORT_VERSION = 1;

    /** 导出密码文件的路径 */
    public final static String PWD_EXPORT_PATH = Environment.getExternalStorageDirectory()
            + File.separator + "pwd_exp.txt";
}
